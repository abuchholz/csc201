// To Compile and Run
// export PATH_TO_FX=~/Downloads/javafx-sdk-14.0.1/lib
// javac --module-path $PATH_TO_FX --add-modules javafx.controls Characters.java
// java --module-path $PATH_TO_FX --add-modules javafx.controls Characters

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Characters extends Application {
    public void start(Stage primaryStage) {
        Pane pane = new Pane();
        pane.setPrefSize(600, 600);

        Font font = Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 35);

        String welcome = "Welcome to Java";
        double rotation = 90;
        double offset = pane.getPrefWidth() / 2;
        double radius = 100;
        double x = offset + Math.cos(rotation) * radius;
        double y = offset + Math.sin(rotation) * radius;

        for (int i = 0; i < welcome.length(); i++) {
            x = offset + Math.cos(Math.toRadians(rotation)) * radius;
            y = offset + Math.sin(Math.toRadians(rotation)) * radius;
            Text text = new Text(x, y, welcome.charAt(i) + "");
            // System.out.println("Actual X" + text.getX());
            // System.out.println("Actual Y" + text.getY());

            text.setFont(font);
            text.setRotate(rotation);
            pane.getChildren().add(text);
            rotation += 22.5;
        }
        Scene scene = new Scene(pane);
        primaryStage.setTitle("Test");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}