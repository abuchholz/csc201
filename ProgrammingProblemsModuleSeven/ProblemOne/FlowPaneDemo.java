// To Compile and Run
// export PATH_TO_FX=~/Downloads/javafx-sdk-14.0.1/lib
// javac --module-path $PATH_TO_FX --add-modules javafx.controls FlowPaneDemo.java
// java --module-path $PATH_TO_FX --add-modules javafx.controls FlowPaneDemo

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.scene.control.Button;

public class FlowPaneDemo extends Application {
    public void start(Stage primaryStage) {
        FlowPane paneOne = new FlowPane();
        FlowPane paneTwo = new FlowPane();
        
        Button buttonOne = new Button("Button One");
        Button buttonTwo = new Button("Button Two");
        Button buttonThree = new Button("Button Three");
        Button buttonFour = new Button("Button Four");
        Button buttonFive = new Button("Button Five");
        Button buttonSix = new Button("Button Six");

        paneOne.getChildren().add(buttonOne);
        paneOne.getChildren().add(buttonTwo);
        paneOne.getChildren().add(buttonThree);

        paneTwo.getChildren().add(buttonFour);
        paneTwo.getChildren().add(buttonFive);
        paneTwo.getChildren().add(buttonSix);

        Scene sceneOne = new Scene(paneOne, 250, 600);
        Scene sceneTwo = new Scene(paneTwo, 320, 400);

        Stage secondaryStage = new Stage();

        primaryStage.setTitle("First Stage");
        primaryStage.setScene(sceneOne);
        secondaryStage.setTitle("Second Stage");
        secondaryStage.setScene(sceneTwo);

        primaryStage.show();
        secondaryStage.show();
    }

    public static void main(String[] args){
        Application.launch(args);
    }
}

