import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.*; 
import java.util.ArrayList;
import javax.swing.ImageIcon;
import java.awt.event.*;  
import java.io.*; 
import java.util.*; 

public class DeckOfCards extends JFrame {
    // Define ImageIcons
    private static ImageIcon image1 = new ImageIcon("images/1.png");
    private static ImageIcon image2 = new ImageIcon("images/2.png");
    private static ImageIcon image3 = new ImageIcon("images/3.png");
    private static ImageIcon image4 = new ImageIcon("images/4.png");
    private static ImageIcon image5 = new ImageIcon("images/5.png");
    private static ImageIcon image6 = new ImageIcon("images/6.png");
    private static ImageIcon image7 = new ImageIcon("images/7.png");
    private static ImageIcon image8 = new ImageIcon("images/8.png");
    private static ImageIcon image9 = new ImageIcon("images/9.png");
    private static ImageIcon image10 = new ImageIcon("images/10.png");
    private static ImageIcon image11 = new ImageIcon("images/11.png");
    private static ImageIcon image12 = new ImageIcon("images/12.png");
    private static ImageIcon image13 = new ImageIcon("images/13.png");
    private static ImageIcon image14 = new ImageIcon("images/14.png");
    private static ImageIcon image15 = new ImageIcon("images/15.png");
    private static ImageIcon image16 = new ImageIcon("images/16.png");
    private static ImageIcon image17 = new ImageIcon("images/17.png");
    private static ImageIcon image18 = new ImageIcon("images/18.png");
    private static ImageIcon image19 = new ImageIcon("images/19.png");
    private static ImageIcon image20 = new ImageIcon("images/20.png");
    private static ImageIcon image21 = new ImageIcon("images/21.png");
    private static ImageIcon image22 = new ImageIcon("images/22.png");
    private static ImageIcon image23 = new ImageIcon("images/23.png");
    private static ImageIcon image24 = new ImageIcon("images/24.png");
    private static ImageIcon image25 = new ImageIcon("images/25.png");
    private static ImageIcon image26 = new ImageIcon("images/26.png");
    private static ImageIcon image27 = new ImageIcon("images/27.png");
    private static ImageIcon image28 = new ImageIcon("images/28.png");
    private static ImageIcon image29 = new ImageIcon("images/29.png");
    private static ImageIcon image30 = new ImageIcon("images/30.png");
    private static ImageIcon image31 = new ImageIcon("images/31.png");
    private static ImageIcon image32 = new ImageIcon("images/32.png");
    private static ImageIcon image33 = new ImageIcon("images/33.png");
    private static ImageIcon image34 = new ImageIcon("images/34.png");
    private static ImageIcon image35 = new ImageIcon("images/35.png");
    private static ImageIcon image36 = new ImageIcon("images/36.png");
    private static ImageIcon image37 = new ImageIcon("images/37.png");
    private static ImageIcon image38 = new ImageIcon("images/38.png");
    private static ImageIcon image39 = new ImageIcon("images/39.png");
    private static ImageIcon image40 = new ImageIcon("images/40.png");
    private static ImageIcon image41 = new ImageIcon("images/41.png");
    private static ImageIcon image42 = new ImageIcon("images/42.png");
    private static ImageIcon image43 = new ImageIcon("images/43.png");
    private static ImageIcon image44 = new ImageIcon("images/44.png");
    private static ImageIcon image45 = new ImageIcon("images/45.png");
    private static ImageIcon image46 = new ImageIcon("images/46.png");
    private static ImageIcon image47 = new ImageIcon("images/47.png");
    private static ImageIcon image48 = new ImageIcon("images/48.png");
    private static ImageIcon image49 = new ImageIcon("images/49.png");
    private static ImageIcon image50 = new ImageIcon("images/50.png");
    private static ImageIcon image51 = new ImageIcon("images/51.png");
    private static ImageIcon image52 = new ImageIcon("images/52.png");


    ArrayList<ImageIcon> list = new ArrayList<ImageIcon>();
    JPanel cardsPanel = new JPanel();
    static Random generator = new Random();
    private static int rand1 = generator.nextInt(52);
    private static int rand2 = generator.nextInt(52);
    private static int rand3 = generator.nextInt(52);
    private static JButton refresh = new JButton("Refresh");

    public DeckOfCards() {
        setLayout(new GridLayout(2, 1));
        add(cardsPanel);
        cardsPanel.setLayout(new FlowLayout(FlowLayout.LEFT,2,2));

        // Add images to ArrayList
        list.add(image1);
        list.add(image2);
        list.add(image3);
        list.add(image4);
        list.add(image5);
        list.add(image6);
        list.add(image7);
        list.add(image8);
        list.add(image9);
        list.add(image10);
        list.add(image11);
        list.add(image12);
        list.add(image13);
        list.add(image14);
        list.add(image15);
        list.add(image16);
        list.add(image17);
        list.add(image18);
        list.add(image19);
        list.add(image20);
        list.add(image21);
        list.add(image22);
        list.add(image23);
        list.add(image24);
        list.add(image25);
        list.add(image26);
        list.add(image27);
        list.add(image28);
        list.add(image29);
        list.add(image30);
        list.add(image31);
        list.add(image32);
        list.add(image33);
        list.add(image34);
        list.add(image35);
        list.add(image36);
        list.add(image37);
        list.add(image38);
        list.add(image39);
        list.add(image40);
        list.add(image41);
        list.add(image42);
        list.add(image43);
        list.add(image44);
        list.add(image45);
        list.add(image46);
        list.add(image47);
        list.add(image48);
        list.add(image49);
        list.add(image50);
        list.add(image51);
        list.add(image52);

        Collections.shuffle(list);
        cardsPanel.add(new JLabel(list.get(1)));
        Collections.shuffle(list);
        cardsPanel.add(new JLabel(list.get(1)));
        Collections.shuffle(list);
        cardsPanel.add(new JLabel(list.get(1)));
        Collections.shuffle(list);
        cardsPanel.add(new JLabel(list.get(1)));

        JPanel buttonPanel = new JPanel(new BorderLayout());
        buttonPanel.add(refresh,BorderLayout.CENTER);
        add(buttonPanel);
        refresh.addActionListener(new CardRefreshButton());
    }

    private class CardRefreshButton implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            cardsPanel.removeAll();
            cardsPanel.add(new JLabel(list.get(1)));
            Collections.shuffle(list);
            cardsPanel.add(new JLabel(list.get(1)));
            Collections.shuffle(list);
            cardsPanel.add(new JLabel(list.get(1)));
            Collections.shuffle(list);
            cardsPanel.add(new JLabel(list.get(1)));
            repaint();
            setVisible(true);
        }  
    } 

    public static void main(String[] args) {
        JFrame frame = new DeckOfCards();
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}