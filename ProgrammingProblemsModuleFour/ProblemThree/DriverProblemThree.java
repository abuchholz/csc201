public class DriverProblemThree {
    public static void main(String[] args) {
        Department math = new Department("Math");
        Department computerscience = new Department("Computer Science");

        ComputerScience cs = new ComputerScience("Computer Science 201", "Introduction to Java", computerscience, 67);
        System.out.println(cs);

        Calculus calc = new Calculus("Calculus I & II", "Continues the study of calculus.", math, 42);
        System.out.println(calc);

        Engineering engineering = new Engineering("Engineering III", "Application of Engineering I & II in Final Project", math, 12);
        System.out.println(engineering);

        // If you want to print the Department and an ArrayList of the courses uncomment these lines
        // System.out.println(math);
        // System.out.println(computerscience);
    }
}