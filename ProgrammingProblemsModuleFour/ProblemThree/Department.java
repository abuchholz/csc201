import java.util.ArrayList;

public class Department {
    String name;
    ArrayList<Courses> courses = new ArrayList<Courses>();

    public Department(String name) {
        this.name = name;
    }

    public void AddCourse(Courses course) {
        courses.add(course); 
    }

    public String toString() {
        String details = "Department Name: " + name + " Courses: " + courses;
        return details;
    }
}