public class Engineering extends Courses {
    // Inherit and use super to reach Courses class
    public Engineering(String title, String desc, Department department, int number) {
        super(title, number, desc, department);
    }
}