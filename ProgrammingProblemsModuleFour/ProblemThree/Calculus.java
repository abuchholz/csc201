public class Calculus extends Courses {
    // Inherit and use super to reach Courses class
    public Calculus(String title, String desc, Department department, int number) {
        super(title, number, desc, department);
    }
}