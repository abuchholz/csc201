public class Courses {
    String title, desc;
    int number;
    Department department; 
    
    // Constructor 
    public Courses(String title, int number, String desc, Department department) {
        this.title = title;
        this.number = number;
        this.desc = desc;
        this.department = department;
        department.AddCourse(this);
    } 

    //prints string of information
    public String toString() {
        String details = "Name of Class: " + title + " | Number of Students: " + number + " | Description of Class: " + desc + " | Department: " + department.name + "\n";
        return details;
    }
}