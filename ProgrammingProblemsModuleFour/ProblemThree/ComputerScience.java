public class ComputerScience extends Courses {
    // Inherit and use super to reach Courses class
    public ComputerScience(String title, String desc, Department department, int number) {
        super(title, number, desc, department);
    }
}