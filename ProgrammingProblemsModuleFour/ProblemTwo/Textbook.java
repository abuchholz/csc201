public class Textbook extends BookTwo {
    // Inherit and use super to reach ReadingType class
    public Textbook(String title, String author, String publication, int pages) {
        super(title, author, publication, pages);
    }
}