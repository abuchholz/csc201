public class Magazine extends ReadingType {
    // Inherit and use super to reach ReadingType class
    public Magazine(String title, String author, String publication, int pages) {
        super(publication, pages, title, author);
    }
}