public class Novel extends BookTwo {
    // Inherit and use super to reach ReadingType class
    public Novel(String title, String author, String publication, int pages) {
        super(title, author, publication, pages);
    }
}