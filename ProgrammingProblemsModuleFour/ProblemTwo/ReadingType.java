public class ReadingType {
    String publication, title, author;
    int pages;
    
    // Constructor 
    public ReadingType(String publication, int pages, String title, String author) {
        this.publication = publication;
        this.pages = pages;
        this.title = title;
        this.author = author;
    } 

    //prints string of information
    public String toString() {
        String details = "Publication: " + publication + " | Pages: " + pages + " | Title: " + title + " | Author: " + author + "\n";
        return details;
    }
}