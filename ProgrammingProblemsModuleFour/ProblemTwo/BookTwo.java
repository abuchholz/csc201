public class BookTwo extends ReadingType {
    // Inherit and use super to reach ReadingType class
    public BookTwo(String title, String author, String publication, int pages) {
        super(publication, pages, title, author);
    }
}