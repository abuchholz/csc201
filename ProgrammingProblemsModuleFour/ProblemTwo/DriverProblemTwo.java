public class DriverProblemTwo {
    public static void main(String[] args) {
        // Renamed to BookTwo because of clashing class names in another practice problem
        BookTwo book = new BookTwo("The Meaning of life", "Joe Smith", "Random Publisher Inc", 42);
        System.out.println(book);

        Magazine magazine = new Magazine("Sony Reveals the new PS5", "Carole Brown", "Real Magazine Company", 29);
        System.out.println(magazine);

        Textbook textbook = new Textbook("Django 101", "John David", "Django Textbook Publishing Company", 385);
        System.out.println(textbook);

        Novel novel = new Novel("1989", "Anna Green", "Ripoff Company ", 254);
        System.out.println(novel);
    }
}