public class Sport {
    private String name;
    private int teamSize;
    private String objectType;

    // Constructor
    public Sport(String name, int teamSize, String objectType) {
        this.name = name;
        this.teamSize = teamSize;
        this.objectType = objectType;
    }

    // toString
    public String toString() {
        String details = "Name: " + name + " | Team Size: " + teamSize + " | Object Type: " + objectType + "\n";
        return details;
    }
}