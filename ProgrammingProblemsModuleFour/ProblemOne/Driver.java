public class Driver {
    public static void main(String[] args) {
        // Define instances and print them
        Soccer soccer = new Soccer(11, "Ball");
        System.out.println(soccer);

        Ultimate ultimate = new Ultimate(7, "Disc");
        System.out.println(ultimate);

        Basketball basketball = new Basketball(5, "Ball");
        System.out.println(basketball);
    }
}