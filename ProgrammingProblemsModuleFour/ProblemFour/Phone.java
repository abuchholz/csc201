public class Phone extends Electronic {
    String carrier;

    // Inherit and use super to reach Courses class
    public Phone(String name, String manufacture, double weight, double cost, int power, String carrier) {
        super(name, manufacture, weight, cost, power);
        this.carrier = carrier;
    }

    public String toString() { 
        String details = super.toString();
        details += " | Carrier " + carrier;
        return details;
    }
}