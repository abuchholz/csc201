public class Computer extends Electronic {
    String type;

    // Inherit and use super to reach Courses class
    public Computer(String name, String manufacture, double weight, double cost, int power, String type) {
        super(name, manufacture, weight, cost, power);
        this.type = type;
    }
    public String toString() { 
        String details = super.toString();
        details += " | Type " + type;
        return details;
    }
}