public class DriverProblemFour {
    public static void main(String[] args) {
        Computer xps = new Computer("XPS 15 7500", "Dell", 4.00, 1799.99, 86, "Laptop");
        System.out.println(xps);

        Computer mac = new Computer("Macbook Pro 16", "Apple", 4.3, 2399.00, 100, "Laptop");
        System.out.println(mac);

        Computer x1 = new Computer("X1 Carbon 7th Gen", "Lenovo", 2.40, 2499.00, 51, "Laptop");
        System.out.println(x1);

        Phone iphone = new Phone("iPhone 12", "Apple", 0.4, 1299.00, 28, "Verizon");
        System.out.println(iphone);
    }
}