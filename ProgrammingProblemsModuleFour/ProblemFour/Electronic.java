public class Electronic {
    String name, manufacture;
    double weight, cost;
    int power;

    // Constructor
    public Electronic(String name, String manufacture, double weight, double cost, int power) {
        this.name = name;
        this.manufacture = manufacture;
        this.weight = weight;
        this.cost = cost;
        this.power = power;
    }

    // toString
    public String toString() {
        String details = "Name " + name + " | Manufacturer " + manufacture + " | Weight " + weight + " | Cost " + cost + " | Battery in WHr " + power;
        return details;
    }
}