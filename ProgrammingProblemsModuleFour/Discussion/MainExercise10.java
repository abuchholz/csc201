import java.util.Scanner;

public class MainExercise10 {
    public static void main(String[] args) {
        // Initialize Instances
        MyStack stack = new MyStack();
        Scanner input = new Scanner(System.in);
        
        // Ask for 5 strings
        System.out.println("Enter 5 strings ->");
        for (int i = 0; i < 5; i++) stack.append(input.next());

        // Print the string backward
        System.out.println("\n");
        while (stack.size() > 0) {
            System.out.println(stack.remove());
        }
    }
}