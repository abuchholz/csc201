import java.util.ArrayList;

public class MyStack extends ArrayList<String> {
    
    public String remove() {
        // Get the last item in the Array List
        String item = get(size() - 1);
        // Remove the last item
        remove(size() - 1);
        // Return the last item
        return item;
    }

    public void append(String item) {
        // Add the string to the ArrayList
        add(item);
    }
}

