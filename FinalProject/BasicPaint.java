import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.*;

public class BasicPaint {
    // Define Global Variables
    public String[] colors = { "Red", "Orange", "Yellow", "Green", "Blue", "Purple", "White", "Black" };
    public JComboBox selection = new JComboBox(colors);
    public JSlider slider = new JSlider(JSlider.VERTICAL, MIN, MAX, INIT);
    public int value = slider.getValue();
    static final int MIN = 3;
    static final int MAX = 25;
    static final int INIT = 15;
    TestPane currentPane;

    public static void main(String[] args) {
        new BasicPaint();
    }

    // Set up GUI Application
    public BasicPaint() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                        | UnsupportedLookAndFeelException ex) {
                    System.out.print("An Error Occured");
                }

                JFrame frame = new JFrame("Pixel Art Editor");
                frame.setPreferredSize(new Dimension(800, 400));
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setLayout(new BorderLayout());
                frame.add(selection, BorderLayout.SOUTH);
                frame.pack();
                frame.add(slider, BorderLayout.WEST);
                frame.pack();
                currentPane = new TestPane();
                frame.add(currentPane);
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);

                // Check if the slider value is changed to rescale the grid
                slider.addChangeListener((ChangeListener) new ChangeListener() {
                    public void stateChanged(ChangeEvent ce) {
                        value = slider.getValue();
                        frame.remove(currentPane);
                        currentPane = new TestPane();
                        frame.add(currentPane);
                        frame.setVisible(true);
                    }
                });
            }
        });
    }

    public class TestPane extends JPanel {
        private static final long serialVersionUID = 1L;

        public TestPane() {
            setLayout(new GridBagLayout());

            // Draw the grid
            GridBagConstraints gbc = new GridBagConstraints();
            for (int row = 0; row < value; row++) {
                for (int col = 0; col < value; col++) {
                    gbc.gridx = col;
                    gbc.gridy = row;

                    CellPane cellPane = new CellPane();
                    Border border = null;
                    if (row < 4) {
                        if (col < 4) {
                            border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
                        } else {
                            border = new MatteBorder(1, 1, 0, 1, Color.GRAY);
                        }
                    } else {
                        if (col < 4) {
                            border = new MatteBorder(1, 1, 1, 0, Color.GRAY);
                        } else {
                            border = new MatteBorder(1, 1, 1, 1, Color.GRAY);
                        }
                    }
                    cellPane.setBorder(border);
                    add(cellPane, gbc);
                }
            }
        }
    }

    public class CellPane extends JPanel {
        private static final long serialVersionUID = 1L;
        public String colorofpixel;
        
        // Color the box when mouse is clicked
        public CellPane() {
            addMouseListener(new MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    colorofpixel = (String) selection.getSelectedItem();
                    // getBackground();
                    if (colorofpixel == "Red") {
                        setBackground(Color.RED); 
                    } else if (colorofpixel == "Orange") {
                        setBackground(Color.ORANGE); 
                    } else if (colorofpixel == "Yellow") {
                        setBackground(Color.YELLOW); 
                    } else if (colorofpixel == "Green") {
                        setBackground(Color.GREEN); 
                    } else if (colorofpixel == "Blue") {
                        setBackground(Color.BLUE); 
                    } else if (colorofpixel == "Purple") {
                        setBackground(Color.MAGENTA);
                    } else if (colorofpixel == "White") {
                        setBackground(Color.WHITE); 
                    } else if (colorofpixel == "Black") {
                        setBackground(Color.BLACK); 
                    }
                }
            });
       }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(300, 300);
        }
    }
}