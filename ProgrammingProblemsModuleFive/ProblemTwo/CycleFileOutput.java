import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class CycleFileOutput {
    private double numberOfWheels, weight;

    public CycleFileOutput(double numberOfWheels, double weight) {
        this.numberOfWheels = numberOfWheels;
        this.weight = weight;
    }

    public String toString() {
        return "number of wheels:" + numberOfWheels + ", weight:" + weight;
    }

    public void outputFile(String message) throws IOException {
        try {
            PrintWriter out = new PrintWriter(
                    new FileWriter("ProgrammingProblemsModuleFive/ProblemTwo/Cycle.txt", true), true);
            out.write(message);
            out.close();
        } catch (IOException e) {
            System.out.println("File not found");
        }
    }
}