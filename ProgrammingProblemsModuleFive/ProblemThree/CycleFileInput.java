import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class CycleFileInput {
    private double numberOfWheels, weight;

    public CycleFileInput(double numberOfWheels, double weight) {
        this.numberOfWheels = numberOfWheels;
        this.weight = weight;
    }

    public String toString() {
        return "number of wheels:" + numberOfWheels + ", weight:" + weight;
    }

    public void outputFile(String message) throws IOException {
        try {
            PrintWriter out = new PrintWriter(
                    new FileWriter("ProgrammingProblemsModuleFive/ProblemThree/Cycle.txt", true), true);
            out.write(message);
            out.close();
        } catch (IOException e) {
            System.out.println("File not found");
        }
    }

    public void readFile() throws FileNotFoundException {
        try {
            File file = new File("ProgrammingProblemsModuleFive/ProblemThree/Cycle.txt");
            Scanner sc = new Scanner(file);

            // we just need to use \\Z as delimiter
            sc.useDelimiter("\\Z");

            System.out.println(sc.next());
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("File not found");
        }
    }
}