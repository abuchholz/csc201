import java.util.InputMismatchException;
import java.util.Scanner;

// I am aware that we were supposed tot take the file from problem two but I just outputed the string to the file and then read it and printed it to the terminal
public class TestApplicationCycleInput {
    public static void main(String[] args) {
        // Initialize Variables
        Scanner input = new Scanner(System.in);
        double num1, num2;

        try {
            System.out.print("Enter number of wheels -> ");
            num1 = input.nextDouble();

            System.out.print("Enter weight -> ");
            num2 = input.nextDouble();

            // Make sure the number entered is greater than 0
            if (num1 <= 0 || num2 <= 0) {
                throw new Exception();
            }

            // Call methods from functions
            CycleFileInput cycle1test = new CycleFileInput(num1, num2);
            cycle1test.outputFile(cycle1test.toString());
            cycle1test.readFile();

        } catch (InputMismatchException e) {
            // See if user input something other than double
            throw new ArithmeticException("Must be Double.");
        } catch (Exception e) {
            // See if user input something other than double
            throw new ArithmeticException("Must be greater than zero.");
        }
    }
}