import java.util.InputMismatchException;
import java.util.Scanner;

public class TestApplicationCycle {
    public static void main(String[] args) {
        // Initialize Variables
        Scanner input = new Scanner(System.in);
        double num1, num2;

        try {
            System.out.print("Enter number of wheels -> ");
            num1 = input.nextDouble();

            System.out.print("Enter weight -> ");
            num2 = input.nextDouble();

            // Make sure the number entered is greater than 0
            if (num1 <= 0 || num2 <= 0) {
                throw new Exception();
            }

            CycleThrowTryCatch cycle1test = new CycleThrowTryCatch(num1, num2);
            System.out.println(cycle1test.toString());

        } catch (InputMismatchException e) {
            // See if user input something other than double
            throw new ArithmeticException("Must be Double.");
        } catch (Exception e) {
            // Catch exeception thrown above
            throw new ArithmeticException("Must greater than 0.");
        }
    }
}