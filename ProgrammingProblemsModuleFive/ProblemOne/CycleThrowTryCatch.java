public class CycleThrowTryCatch {
    private double numberOfWheels, weight;

    public CycleThrowTryCatch(double numberOfWheels, double weight) {
        this.numberOfWheels = numberOfWheels;
        this.weight = weight;
    }

    public String toString() {
        return "number of wheels:" + numberOfWheels + ", weight:" + weight;
    }
}