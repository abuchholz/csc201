
// Chapter 12 | Problem 19 | Page 516
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class DiscussionFive {
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);

        // Get Author Input
        System.out.print("Enter Author ->");
        String author = input.nextLine();
        // Get Author File Name
        System.out.print("Enter Path to File (Authors.txt) ->");
        String authorfilestring = input.nextLine();
        File authorfile = new File(authorfilestring);
        // Write to File
        writeInFile(author, authorfile);

        // Get Book Input
        System.out.print("Enter Book Title ->");
        String book = input.nextLine();
        // Get Book File Input
        System.out.print("Enter Path to File (Books.txt) ->");
        String bookfilestring = input.nextLine();
        File bookfile = new File(bookfilestring);
        // Append Line of author to book
        book = book + ":" + readInFile(author, authorfile);
        // Write to File
        writeInFile(book, bookfile);
    }

    public static void writeInFile(String name, File file) {
        // Attempts to print if not successfual catches exception
        try {
            PrintWriter writer = new PrintWriter(new FileWriter(file, true));
            writer.write(name);
            writer.write(System.getProperty("line.separator"));
            writer.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static int readInFile(String name, File file) throws IOException {
        // Used to find the line the author is on
        BufferedReader reader = new BufferedReader(new FileReader(file));
        int lines = 0;
        String s;
        while ((s = reader.readLine()) != null) {
            lines++;
            if (s.equals(name)) {
                break;
            }
        }
        reader.close();
        return lines;
    }
}