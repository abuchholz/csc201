/*
    class ProblemThree
        months[] temps[][]
    
        main
            instance of ProblemThree
            instance.inputTempForYear
            instance.inputTempForMonth

            print "average high" + averagehugh
            print for the rest of the methods

        method inputtempformonth
            input high
            temps[0][i] = high
            input low 
            temps[1][i] = high

        method inputTempForYear
            return 2d array
        
        method calculateAverageHigh(2d array)
            for i in 2d array
                temp += temp[0][]
                temp / temp.length

        method calculateAverageLow(2d array)
            for i in 2d array
                temp += temp[1][]
                temp / temp.length

        method findHighestTemp(2d array)
            int max, temp
            for i in 2d array length
                temp = array[0][i]
                if temp > max
                    max=temp[0][i]

        method findLowestTemp(2d array)
            int min, temp 
            for i in 2d array length
                temp = array[0][i]
                if temp > min
                    min=array[0][i]
*/         

import java.util.Scanner;

public class ProblemThree {

    String months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    int temps[][] = new int[2][12];

    public static void main(String[] args) {

        ProblemThree instance = new ProblemThree();
        instance.inputTempForYear();
        instance.inputTempForMonth();

        System.out.println("The average high of the year is: " +  instance.calculateAverageHigh(instance.inputTempForYear()));
        System.out.println("The average low of the year is: " + instance.calculateAverageLow(instance.inputTempForYear()));
        System.out.println("The highest temperature is: " + instance.findHighestTemp(instance.inputTempForYear()));
        System.out.println("The lowest temperature is: " + instance.findLowestTemp(instance.inputTempForYear()));
    }

    public void inputTempForMonth() {
        Scanner input = new Scanner(System.in); 
        
        for(int i = 0; i < months.length; i++) {
            System.out.print("Enter High for " + months[i] + " -> ");
            int high = input.nextInt();
            temps[0][i] = high;
            System.out.print("Enter Low for " + months[i] + " -> ");
            int low = input.nextInt();
            temps[1][i] = low;
            System.out.print("\n");
        }
    }

    public int[][] inputTempForYear() {
        return temps;
    }

    public double calculateAverageHigh(int[][] temps) { 
        double total = 0;
        for (int i = 0; i < temps[0].length; i++) {
            total += temps[0][i];
        }
        return total / temps[0].length;
    }
    
    public double calculateAverageLow(int[][] temps) {  
        double total = 0;
        for (int i = 0; i < temps[1].length; i++) {
            total += temps[1][i];
        }
        return total / temps[1].length;
    }
    
    public int findHighestTemp(int[][] temps) {
        int temp;
        int max = 0;
        for (int i = 0; i < temps[0].length; i++) {
            temp = temps[0][i];
            if (temp > max) {
                max = temps[0][i];
            }
        }
        return max;
    }
    
    public int findLowestTemp(int[][] temps) {
        int temp;
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < temps[1].length; i++) {
            temp = temps[1][i];
            if (min > temp) {
                min = temps[1][i];
            }
        }
        return min;
    }
}