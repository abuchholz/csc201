// This program will allow a Girl Scout Troup leader determine how many boxes a girl sold.

/*
input number of girls
array girlScout, totalBoxes, values
for i in input number of girls
    input number of boxes
if boxes <= 10
    totalboxes[0] + 1
    ... remaining else if and else statments
for i in totalBoxes
    print values[i] and totalboxes[i]
*/

import java.util.Scanner; // import the Scanner class

public class ProblemOne {
    public static void main(String args[]){
        Scanner input = new Scanner(System.in); 

        // Ask array length
        System.out.print("Total number of Girls in Troop -> ");
        int num = input.nextInt();

        // Initialize array
        int girlScout[] = new int[num];
        int totalboxes[] = {0,0,0,0,0};
        String values[] = {"0-10", "11-20", "21-30", "31-40", "41+"};  

        for (int i = 1 ; i <= girlScout.length; i++ ) {
            System.out.print("Boxes of cookies for girl #" + i + " -> ");
            girlScout[i-1] = input.nextInt();
        }

        for(int i = 0; i < girlScout.length; i++) { 
            int boxes = girlScout[i];
                if (boxes <= 10) {
                    totalboxes[0] = totalboxes[0] + 1; 
                } else if (boxes <= 20) {
                    totalboxes[1] = totalboxes[1] + 1; 
                } else if (boxes <=30) {
                    totalboxes[2] = totalboxes[2] + 1;
                } else if (boxes <=40) {
                    totalboxes[3] = totalboxes[3] + 1;  
                } else {
                    totalboxes[4] = totalboxes[4] + 1; 
                }   
            }
        System.out.print("\n");
        for (int i = 0; i < totalboxes.length; i++) {  
            System.out.println(values[i] + "\t\t" + totalboxes[i]); 
        }

        // Destroy the instance after use
        input.close();
    }
}