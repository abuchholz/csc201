/*
method inputValidScore(int)
    input score
    if < 0 || if > 10
        print "invalid score"

method inputAllScores
    array double score
    for i in 7
        score[i] equals inputValidScore(judge + 1)

method inputValidDegreeOfDifficulty
    input difficulty
    if difficulty < 1.2 || difficulty > 3.8
        print "invalid"

method calculateScore
    double difficulty, double scores[]
    for find max
        max = i
    for find min
        min = i
    
    for i in scores.length
        sum += score[i]

    result = sum * difficulty * .6
    print result
*/

import java.util.Scanner;

public class ProblemTwo {
    public static void main(String args[]){
        ProblemTwo instance = new ProblemTwo();     
        instance.calculateScore();
    }

    // Check if score is valid
    public double inputValidScore(int judge) {
        // Create Scanner instance
        Scanner input = new Scanner(System.in); 

        // Determine if Input is Invalid
        double inputscore;
        do {
            System.out.print("Judge #" + judge + " score -> ");
            inputscore = input.nextDouble();
            
            if(inputscore < 0 || inputscore > 10) {
                System.out.println("Invalid Score");
            } 
        } while(inputscore < 0 || inputscore > 10);
        
       return inputscore;
    }

    // Input Score into Array
    public double[] inputAllScores() {
        double score[] = new double[7];

        for (int i = 0 ; i < score.length; i++ ) {
            score[i] = inputValidScore(i + 1);
        }
        return score;     
    }  

    // Input Difficulty
    public double inputValidDegreeOfDifficulty() {
        Scanner input = new Scanner(System.in); 

        double difficulty;
        do { 
            System.out.print("Enter Degree of Difficulty -> ");
            difficulty = input.nextDouble();

            if(difficulty < 1.2 || difficulty > 3.8) {
                System.out.println("Invalid input!");
            }
        } while(difficulty <= 1.2 || difficulty >= 3.8);

        return difficulty;
    }

    // Calculate Score
    public void calculateScore() {
        double difficulty = inputValidDegreeOfDifficulty();
        double scores[] = inputAllScores();

        // Remove max score
        int max = 0;
        for(int i = 1; i < scores.length; i++) {
            if (scores[i] > scores[max]) {
                max = i;
            }
        }    
        // Remove min score
        int min = 0;
        for(int i = 1; i < scores.length; i++) {
            if (scores[i] < scores[min]) {
                min = i;
            }
        }

        double sum = 0;
        for (int i = 0; i < scores.length; i++) {
            if (i != min && i != max) {
                sum += scores[i];
            }
        }

        double result = sum * difficulty * 0.6;
        System.out.println("\n\n" + result);
    }
}