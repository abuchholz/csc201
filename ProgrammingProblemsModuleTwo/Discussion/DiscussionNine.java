// Chapter 7 | Question9 | Page 302

import java.util.Scanner;

class DiscussionNine {
    public static void main(String[] args) {
        // Create a Scanner instance
        Scanner input = new Scanner(System.in);

        // Declare Array
        double[] numbers = {0,0,0,0,0,0,0,0,0,0}; 

        // Ask for input 
        System.out.print("Enter ten numbers: ");

        // Ask for inputs
        for(int i = 0; i < 10; i++) {
            double inputscore = input.nextDouble();
            numbers[i] = inputscore;
        }

        // Use method to find max value
        double maxnum = max(numbers);

        // Print Max Num
        System.out.println("The maximum number is " + maxnum);
    }

    public static double max(double[] array) {
        double max = array[0];
        for(int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }  
        return max;
    }
}

// Output
// Enter ten numbers: 1.9 2.5 3.7 2 1.5 6 3 4 5 2
// The maximum number is 6.0

// Explanation
// 1. Initialize Scanner Instance
// 2. Declare array with a length of 10
// 3. Ask user to type 10 numbers
// 4. Pass array as a parameter in the max method. 
// 5. The max method then runs through the list and if the index is larger than the last it replaces the variable max

// I figured this out because I had to do it in the Diving Practice Problem. I determined that it would be easy if I ran through the array and replaced a variable when it found a larger number.

// Question
// public static double max(double[] array)
// Write a test program that prompts the user to enter ten numbers, invokes this method to return the maximum value, and displays the maximum value. Here is a sample run of the program:
