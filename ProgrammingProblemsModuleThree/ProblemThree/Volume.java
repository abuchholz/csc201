/*
class Volume
    private Book class book array
    private numofBooks int
    private volumeName string

    method Volume(book, numofBooks, volumeName)
        this.book = book
        ... do this for remaining parameters

    method getBookArray
        String booklist
        for Book i:book
            booklist += i

        return bookList
    
    method toString
        return "books" + Arrays.toString(book) ... return remaining variables

*/

import java.util.Arrays;

public class Volume {
    private Book book[];
	private int numberOfBooks;
    private String volumeName;
    
    public Volume(Book book[], int numberOfBooks, String volumeName) {
		this.book = book;
		this.numberOfBooks = numberOfBooks;
		this.volumeName = volumeName;
    }
    
    public String getBookArray() {
		String bookList="";
		for(Book i:book) {
			bookList += i +"\n";
		}
		return bookList;
    }
    
    public String toString() {
		return "books:" + Arrays.toString(book) + ", numberOfBooks:" + numberOfBooks + ", volumeName:" + volumeName;
	}
}