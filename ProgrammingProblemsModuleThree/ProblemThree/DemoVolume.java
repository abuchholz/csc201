/*
method main
    Book books[] = {new Book(), new Book()}

    Volume volume1 = new Volume()

    print volume1.toString
    print volume1.getBookArray
*/

public class DemoVolume {
    public static void main(String[] args) {
        // Create array books with two instances of Book
        Book books[] = {new Book("Ascend Online", "Luke Chmilenko", 644), new Book("Ready Player One", "Ernest Cline", 579)};

        // Create insance of Volume with array of books
        Volume volume1 = new Volume(books, 2, "LitRPG Volume");
        
        // Display Content
        System.out.println(volume1.toString());
        System.out.println("\n");
        System.out.println(volume1.getBookArray());
    }
}