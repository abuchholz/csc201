/*
class Book
    private String title, author
    private int numofPages

    method Book(title author numofPages)
        this title = title
        ... do this for author and numofPages

    method toString
        return "title" + title ... do this for rest of variables 
*/

public class Book {
    private String title;
	private String author;
	private int numberOfPages;
	public Book(String title, String author, int numberOfPages) {
		this.title = title;
		this.author = author;
		this.numberOfPages = numberOfPages;
    }
    public String toString() {
		return "title:" + title + ", author:" + author + ", numberOfPages:" + numberOfPages;
	}
}