/*
class Cycle
    int numberofwheels, weight

    method Cycle(numberofwheels, weight)
        this.numberofwheels = numberofwheels
        ... same for weight

    method Cycle()
        this(100,1000)
    
    method toString
        return "num of wheels" + numberofwheels ... also return weight 
*/

public class Cycle {
    private int numberOfWheels, weight;

    public Cycle(int numberOfWheels, int weight) {
        this.numberOfWheels = numberOfWheels;
        this.weight = weight;
    }

    public Cycle() {
        this(100, 1000);
    }

    public String toString() {
        return "number of wheels:" + numberOfWheels + ", weight:" + weight;
    }
}