import java.io.ObjectInputFilter.Status;

/*
method main 
    Cycle cycletest = new Cycle(2, 200)
    print cycletest.toString

    Cycle cycletest2 = new Cycle()
    print cycletest2.toString
*/

public class TestApplication {
    public static void main(String[] args) {
        // Create object and pass parameters
        Cycle cycle1test = new Cycle(2, 200);
        System.out.println(cycle1test.toString());

        // Call constructor with no parameters that has default values
        Cycle cycle2test = new Cycle();
        System.out.println(cycle2test.toString());
    }
}