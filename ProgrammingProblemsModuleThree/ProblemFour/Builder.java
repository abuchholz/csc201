/*
method main
    instance Scanner input
    print "First String"
    String message
    message += new String

    print "Type 'yes,' ->""
    String yes

    instance StringBuilder stringbuilder

    print stringbuilder capacity
    stringbuilder append " I love it!"

    stringbuilder insert (message length, yes)
    print stringbuilder
*/

import java.util.Scanner; 

public class Builder {
	public static void main(String[] args) {
        // Create an instance of Scanner
        Scanner input = new Scanner(System.in);

        // Ask for input "Java is Fun!"
        System.out.print("First String -> ");
        String message =  input.nextLine();
        // Add space so that Yes is not next to !
        message += new String(" ");
                
        // Add yes to StringBuilder
        System.out.print("Type 'Yes,' -> ");
        String yes =  input.nextLine();

        // Create an instance of StringBuilder
		StringBuilder stringbuilder = new StringBuilder(message);
        
        // Print capacity 
        System.out.println("Initial capacity " + stringbuilder.capacity());
        
        // Append Love Java
        stringbuilder.append(" I love it!");

        stringbuilder.insert(message.length(), yes);

        // Print capacity 
        System.out.println("Final capacity " + stringbuilder.capacity() + "\n");
        
        // Print StringBuilder
		System.out.println(stringbuilder);
	}
}