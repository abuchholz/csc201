/*
method main

declare BufferedReader, length, tmp, c[]

for i in string length
    print charAT i

    if Character isDigit
        string replace "*"
        print "digit"
    else if Character isUpperCase
        string setChar toLowerCase
        print "char"
    else if Character isLowerCase
        string setchar toUpperCase
        print "char"
    else 
        print "Space"

print string
*/

import java.io.*;
import java.util.*; 

public class CharacterArray {
    public static void main(String[] args) throws IOException {
        // Get Input
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String tmp = br.readLine();
        int length = tmp.length();
        char c[] = new char[length];
        tmp.getChars(0, length, c, 0);

        // Go through Char Array and print if digit or char and replace
        for (int i=0; i < length; i++) {
            System.out.print(c[i]);
            if (Character.isDigit(c[i])) {
                System.out.print(" Digit ");
                c[i] = '*';
            } else if (Character.isUpperCase(c[i])) {
                System.out.print(" Uppercase Character ");
                c[i] = Character.toLowerCase(c[i]);
            } else if (Character.isLowerCase(c[i])) {
                System.out.print(" Lowercase Character ");
                c[i] = Character.toUpperCase(c[i]);
            } else {
                System.out.print(" Space ");
            }
            for (int j=0; j < length; j++) {
                System.out.print(c[j]);
            }
            System.out.println("");
        }
    }
}
