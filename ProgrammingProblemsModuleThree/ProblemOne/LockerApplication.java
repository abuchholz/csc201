import java.util.Scanner;

public class LockerApplication {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        // Instance of Mickey Mouse
        CombinationLock MickeyCombination = new CombinationLock(28, 17, 39);
        Locker MickeyLocker = new Locker(100, "Mickey Mouse", 3, MickeyCombination);

        // Ask for combination until correct
        boolean resultmickey = false;
        while (resultmickey == false) {
            resultmickey = MickeyLocker.openLocker();
        } 

        // Ask for the number of book to add 
        System.out.print("Enter number of books you would like to add -> "); 
        int numberofbooksmickeyadd = input.nextInt();
        for (int i = 0; i < numberofbooksmickeyadd; i++) {
            MickeyLocker.putBookInLocker();
        }

        // Ask for the number of book to be removed 
        System.out.print("Enter number of books you would like to remove -> "); 
        int numberofbooksmickeyremove = input.nextInt();
        for (int i = 0; i < numberofbooksmickeyremove; i++) {
            MickeyLocker.removeBookFromLocker();
        }

        // Instance of Donald Duck
        CombinationLock DonaldCombination = new CombinationLock(35, 16, 27);
        Locker DonaldLocker = new Locker(275, "Donald Duck", 0, DonaldCombination);

        // Ask for combination until correct Donald Duck
        System.out.println("\nDonald Duck");
        boolean resultdonald = false;
        while (resultdonald == false) {
            resultdonald = DonaldLocker.openLocker();
        } 

        // Ask for the number of book to add 
        System.out.print("Enter number of books you would like to add -> "); 
        int numberofbooksdonaldadd = input.nextInt();
        for (int i = 0; i < numberofbooksdonaldadd; i++) {
            DonaldLocker.putBookInLocker();
        }

        // Ask for the number of book to be removed 
        System.out.print("Enter number of books you would like to remove -> "); 
        int numberofbooksdonaldremove = input.nextInt();
        for (int i = 0; i < numberofbooksdonaldremove; i++) {
            DonaldLocker.removeBookFromLocker();
        }


        // Print out locker properties and locker combination
        System.out.println("\nResults");
        System.out.println(MickeyLocker);
        System.out.println(DonaldLocker);
	}

}