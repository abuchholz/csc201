public class CombinationLock {
    // Define locker numbers
    int number1, number2, number3;
    int currentPosition = 0;
    
    // Constructor
	public CombinationLock(int number1, int number2, int number3) {
		this.number1 = number1;
		this.number2 = number2;
		this.number3 = number3;
	}

    // Reset the locker combionation 
    public void resetDial() {
        currentPosition = 0;
    }
    
    // Turn the lock right 
    public void turnRight(int number) {
        currentPosition = currentPosition - number;
        if (currentPosition < 0) {
            currentPosition += 40;
        }
        System.out.println("Turned Right to Number " + currentPosition);
    }

    // Turn the lock left
	public void turnLeft(int number) {
        currentPosition = currentPosition + number;
        currentPosition = currentPosition % 40;
        System.out.println("Turned Left to Number " + currentPosition);
    }

    // Turn right, turn left, turn right, then determine if locker number was correct
    public boolean openLock(int number1, int number2, int number3) {
        boolean opened = true;
        resetDial();
        turnRight(number1);
        if (currentPosition != this.number1)
            opened = false;
        turnLeft(number2);
        if (currentPosition != this.number2)
            opened = false;
        turnRight(number3);
        if (currentPosition != this.number3)
            opened = false;
        return opened;
    }

    // Print locker number info
	public String toString() {
		return "number1:" + number1 + ", number2:" + number2 + ", number3:" + number3;
	}
}