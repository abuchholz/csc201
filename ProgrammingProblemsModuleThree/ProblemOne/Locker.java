import java.util.Scanner;

public class Locker {
    // Define properties of locker
	int lockerNumber, numberOfBooks;
    String studentName;
    
    // Create private instance
	private CombinationLock combination = new CombinationLock(0,0,0);

    // Constructor of Locker
	public Locker(int lockerNumber, String studentName, int numberOfBooks, CombinationLock combination) {
		this.lockerNumber = lockerNumber;
        this.studentName = studentName;
        this.numberOfBooks = numberOfBooks;
		this.combination = combination;
	}

    // Add books to locker
	public void putBookInLocker() {
		this.numberOfBooks = this.numberOfBooks + 1;
    }

    // Remove books from locker
    public boolean removeBookFromLocker() {
        if (this.numberOfBooks == 0) {
            System.out.println("Can't remove any more books because no more books exist");
            return false;
        } else {
            this.numberOfBooks = this.numberOfBooks - 1;
            return true;
        }
	}

    // Open the locker and print opened or not opened
    public boolean openLocker() {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter first number -> "); 
        int number1 = input.nextInt();
        System.out.print("Enter second number -> "); 
        int number2 = input.nextInt();
        System.out.print("Enter third number -> "); 
        int number3 = input.nextInt();
        boolean opened = combination.openLock(number1, number2, number3);
        System.out.println("The lock was " + ((opened) ? "opened" : "not opened"));
        return opened;
    }

    // Print properties of locker and combination
	public String toString() {
		return "lockerNumber:" + lockerNumber + ", studentName:" + studentName + ", numberOfBooks:" + numberOfBooks + " " + combination;
	}
}