import java.math.BigDecimal;

public class RegularPay extends PayCalculator {
    public RegularPay(BigDecimal payRate) {
        super(payRate);
    }

    public BigDecimal computePay(float hour) {
        return (payRate.multiply(new BigDecimal(hour)));
    }
}