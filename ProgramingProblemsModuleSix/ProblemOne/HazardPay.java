import java.math.BigDecimal;

public class HazardPay extends PayCalculator {
    public HazardPay(BigDecimal payRate) {
        super(payRate);
    }

    public BigDecimal computePay(float hour) {
        return payRate.multiply(new BigDecimal(hour)).multiply(new BigDecimal(1.5));
    }
}