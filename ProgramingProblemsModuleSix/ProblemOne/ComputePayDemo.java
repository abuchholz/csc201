import java.math.BigDecimal;

public class ComputePayDemo {
    public static void main(String[] args) {
        BigDecimal payRate = new BigDecimal(7.35);
        RegularPay rp = new RegularPay(payRate);
        System.out.println(rp.computePay(7));

        PayCalculator pay = new HazardPay(payRate);
        System.out.println(pay.computePay(5));

    }
}