import java.util.Scanner;

public class MessageEncoderDemo {
    public static void main(String[] args) {
        // SubstitutionCipher
        Scanner input = new Scanner(System.in);
        System.out.print("Enter message : ");
        String PlainText = input.nextLine();
        System.out.print("Enter Shift : ");
        int shift = input.nextInt();
        SubstitutionCipher t = new SubstitutionCipher(shift);
        String s = t.encode(PlainText);
        System.out.println("Message :" + s);
        // Decode
        String decoded = t.decode(s);
        System.out.println("Decoded text after removing cipher: " + decoded);

        // ShuffleCipher
        Scanner input2 = new Scanner(System.in);
        System.out.print("\nEnter message : ");
        String PlainTextShuffle = input2.nextLine();
        System.out.print("Number of Shuffle : ");
        int shuffle = input2.nextInt();
        ShuffleCipher t2 = new ShuffleCipher(shuffle);
        String s2 = t2.encode(PlainTextShuffle);
        System.out.println("Shuffled message is :" + s2);
        // Decode
        String decoded2 = t2.decode(s2);
        System.out.println("Decoded Text: " + decoded2);
    }
}