class ShuffleCipher implements MessageEncoder, MessageDecoder {
    int n;

    ShuffleCipher(int n) {
        this.n = n;
    }

    private String OneShuffle(String s) {
        int len = s.length();
        int half, len1, len2, i;
        char c;
        String first_half = "", second_half = "", ans = "";

        if (len % 2 != 0) {
            half = (len / 2) + 1;
        } else
            half = len / 2;

        for (i = 0; i < half; i++) {
            c = s.charAt(i);
            first_half += c;
        }
        int j = 0;
        for (i = half; i < len; i++) {
            c = s.charAt(i);
            second_half += c;
        }

        len1 = first_half.length();
        len2 = second_half.length();

        if (len1 == len2) {
            for (i = 0; i < len1; i++) {
                c = first_half.charAt(i);
                ans += c;
                c = second_half.charAt(i);
                ans += c;
            }
        } else {
            for (i = 0; i < len2; i++) {
                c = first_half.charAt(i);
                ans += c;
                c = second_half.charAt(i);
                ans += c;
            }
            c = first_half.charAt(i);
            ans += c;
        }
        return ans;
    }

    public String encode(String plainText) {
        String ans = "";
        ans = OneShuffle(plainText);
        for (int i = 0; i < n - 1; i++) {
            ans = OneShuffle(ans);
        }
        return ans;
    }

    public String decode(String cipherText) {
        String plainText = cipherText;
        for (int i = 0; i < n; i++) {
            plainText = unshuffle(plainText);
        }
        return plainText;
    }

    private String unshuffle(String s) {
        String unshuffled = "";

        for (int i = 0; i < s.length(); i += 2)
            unshuffled = unshuffled + s.charAt(i);
        for (int i = 1; i < s.length(); i += 2)
            unshuffled = unshuffled + s.charAt(i);
        return unshuffled;
    }
}