class SubstitutionCipher implements MessageEncoder, MessageDecoder {
    int shift;

    SubstitutionCipher(int shift) {
        this.shift = shift;
    }

    private char ShiftSingleCharacter(char s) {
        return ((char) (((s - 'a' + this.shift) % 26) + 'a'));
    }

    public String encode(String plainText) {
        String ans = "";
        for (char c : plainText.toCharArray()) {
            ans += Character.toString((ShiftSingleCharacter(c)));
        }
        return ans;
    }

    public String decode(String cipherText) {
        String plainText = "";
        for (int i = 0; i < cipherText.length(); i++) {
            Character decodeLetter = cipherText.charAt(i);
            plainText = plainText + shift1CharacterBack(decodeLetter);
        }
        return plainText;
    }

    private Character shift1CharacterBack(Character oneLetter) {
        return (char) (oneLetter - shift);
    }
}