// Chapter 13 | Problem 10 | Page 558
public class DiscussionModuleSix {
    public static void main(String[] args) {

        Rectangle rec1 = new Rectangle(10, 10);
        Rectangle rec2 = new Rectangle(10, 10);
        System.out.println(rec1.equals(rec2));
        System.out.println(rec1.compareTo(rec2));
    }

    private static class Rectangle extends GeometricObject {
        private double width;
        private double height;

        public Rectangle(double width, double height) {
            this.width = width;
            this.height = height;
        }

        /** Return perimeter */
        public double getPerimeter() {
            return 2 * (width + height);
        }

        /** Return area */
        public double getArea() {
            return width * height;
        }
        public boolean equals(Object o) {
            return o instanceof Rectangle && getArea() == ((Rectangle) o).getArea();
        }
    }
}