import java.io.FileWriter;
import java.io.IOException;

public class FractionDemo {
    public static void main(String[] args) {
        Fraction fraction1 = new Fraction(2, 4);
        Fraction fraction2 = new Fraction(4, 9);
        Fraction fraction3 = new Fraction(1, 5);

        for (int i = 0; i < 3; i++) {
            try {
                FileWriter myObj = new FileWriter("ProgrammingProblemsModuleEight/ProblemOne/SerialF.dat");
                myObj.write("Fraction 1 " + fraction1 + "\n");
                myObj.write("Fraction 2 " + fraction2 + "\n");
                myObj.write("Fraction 3 " + fraction3 + "\n");
                myObj.close();
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
    }
}