import java.io.Serializable;

public class Fraction implements Serializable {
    private static final long serialVersionUID = 1L;
    private int numerator;
    private int denominator;
    private static char slash;

    public Fraction(int numerator, int denominator) {
        super();
        Fraction.slash = '/';
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    @Override
    public String toString() {
        return "|" + numerator + slash + denominator + "|";
    }
}