// Chapter 17 | Problem 19 | Page 740

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class HexRepresentation {
    static char[] HEX_ARRAY = "".toCharArray();

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter File Name -> ");
        String file = scanner.nextLine();
        try {
            String content = Files.readString(Paths.get(file));
            HEX_ARRAY = content.toCharArray();
            System.out.println(bytesToHex(HEX_ARRAY));
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found");
        }
    }

    public static String bytesToHex(char[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
}
