import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class ReadMe {
    public static void main(String[] args) {
        try {
            RandomAccessFile pledge = new RandomAccessFile("ProgrammingProblemsModuleEight/ProblemTwo/pledge.txt",
                    "rw");
            pledge.seek(124);
            System.out.println(pledge.readLine());
            pledge.seek(135);
            System.out.println(pledge.readLine());
            pledge.close();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found");
        } catch (IOException e) {
            System.out.println("File Error");
        }
    }

}