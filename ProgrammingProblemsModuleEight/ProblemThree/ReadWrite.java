import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class ReadWrite {
    static int NumberOfStudents;

    public static void GetUserInputs() {
        Scanner scanner = new Scanner(System.in);
        File file = new File("ProgrammingProblemsModuleEight/ProblemThree/Stu.dat");
        System.out.print("Enter # of Students -> ");
        NumberOfStudents = scanner.nextInt();

        try {
            RandomAccessFile StuFile = new RandomAccessFile(file, "rw");
            for (int i = 0; i < NumberOfStudents; i++) {
                System.out.println("Student #" + (i + 1));
                System.out.print("Enter ID -> ");
                int id = scanner.nextInt();
                System.out.print("Enter GPA -> ");
                double gpa = scanner.nextDouble();
                StuFile.writeUTF(id + " " + gpa + "\n");
            }
            StuFile.close();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found");
        } catch (IOException e) {
            System.out.println("An Error Occured");
        }
    }

    public static void ReadFile() {
        int ids[] = new int[6];
        double gpas[] = new double[6];
        File file = new File("ProgrammingProblemsModuleEight/ProblemThree/Stu.dat");
        try {
            RandomAccessFile StuFile = new RandomAccessFile(file, "rw");
            for (int i = 0; i < NumberOfStudents; i++) {
                String lineinfo = StuFile.readUTF();
                String datas[] = lineinfo.split(" ");
                ids[i] = Integer.parseInt(datas[0]);
                gpas[i] = Double.parseDouble(datas[1]);
            }
            StuFile.close();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found");
        } catch (IOException e) {
            System.out.println("An Error Occured");
        }

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Enter Student ID For GPA -> ");
            int id = scanner.nextInt();
            boolean exists = false;
            for (int i = 0; i < NumberOfStudents; i++) {
                if (id == ids[i]) {
                    System.out.println("Student's ID -> " + ids[i]);
                    System.out.println("Student's GPA -> " + gpas[i]);
                    exists = true;
                    break;
                }
                if (!exists) {
                    System.out.println("No Student Found");
                }
            }
        }
    }

    public static void main(String[] args) {
        GetUserInputs();
        ReadFile();
    }
}