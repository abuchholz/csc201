import java.util.*; 

public class ProblemTwoTest {
    	public static void main(String[] args) {
            ProblemTwo test1 = new ProblemTwo();
            test1.vendingMachineStock.put("Water", 1.00);

            // Test 1 | 5 Dollars | 5 Waters
            List<Integer> moneyvalues1 = new ArrayList<>(Arrays.asList(1)); 
            List<String> selection1 = new ArrayList<>(Arrays.asList("Water", "Water", "Water", "Water", "Water")); 

            test1.addMoney(moneyvalues1);
            System.out.println("You added " + test1.money + " to the vending machine");
            test1.vendingMachine(selection1);

            System.out.println("\n");


            // Test 2 | 4 Dollars | 2 Doritos 1 Gum
            ProblemTwo test2 = new ProblemTwo();
            test2.vendingMachineStock.put("Doritos", 1.00);
            test2.vendingMachineStock.put("Gum", 0.75);

            List<Integer> moneyvalues2 = new ArrayList<>(Arrays.asList(2,2,2,2)); 
            List<String> selection2 = new ArrayList<>(Arrays.asList("Doritos", "Doritos", "Gum")); 

            test2.addMoney(moneyvalues2);
            System.out.println("You added " + test2.money + " to the vending machine");
            test2.vendingMachine(selection2);

            System.out.println("\n");


            // Test 3 | 2 Dollar 32 Cents | 1 Gum 1 Candy 1 Water
            ProblemTwo test3 = new ProblemTwo();
            test3.vendingMachineStock.put("Water", 1.00);
            test3.vendingMachineStock.put("Gum", 0.75);
            test3.vendingMachineStock.put("Candy", 0.5);

            List<Integer> moneyvalues3 = new ArrayList<>(Arrays.asList(2,2,3,5,6,6)); 
            List<String> selection3 = new ArrayList<>(Arrays.asList("Gum", "Candy", "Water")); 

            test3.addMoney(moneyvalues3);
            System.out.println("You added " + test3.money + " to the vending machine");
            test3.vendingMachine(selection3);

            System.out.println("\n");


            // Test 3 | 1 Dollar 26 Cents | 1 Gum 1 Candy 1 Water
            ProblemTwo test4 = new ProblemTwo();
            test4.vendingMachineStock.put("Water", 1.00);
            test4.vendingMachineStock.put("Gum", 0.75);
            test4.vendingMachineStock.put("Candy", 0.5);

            List<Integer> moneyvalues4 = new ArrayList<>(Arrays.asList(2,3,6)); 
            List<String> selection4 = new ArrayList<>(Arrays.asList("Gum", "Candy", "Water")); 

            test4.addMoney(moneyvalues4);
            System.out.println("You added " + test4.money + " to the vending machine");
            test4.vendingMachine(selection4);


		}
	}