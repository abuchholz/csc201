import java.util.Scanner; 
import java.util.HashMap;
import java.util.*;

// I didn't really understand some of the instructions. I did the best I could from what I understood.

class ProblemTwo{
    // Declare array to save money 
    double money = 0;
    double[] coins = {0, 5, 1, .25, .1, .05, .01};
    
    // Initialize Variables
    HashMap<String, Double> vendingMachineStock = new HashMap<String, Double>();

    public static void main(String args[]){  
        ProblemTwo instance = new ProblemTwo();
        instance.keyboardinput();
    }

    public void keyboardinput() {
        // Create scanner instance
        Scanner input = new Scanner(System.in);

        // Fill Vending Machine with the Goods.
        vendingMachineStock.put("Doritos", 1.0);
        vendingMachineStock.put("Gum", 0.75);
        vendingMachineStock.put("Nuts", 0.25);
        vendingMachineStock.put("Candy", 0.50);
        vendingMachineStock.put("Water", 1.00);

        List<Integer> moneyvalues = new ArrayList<>(); 
        List<String> selections = new ArrayList<>(); 

        for(int i = 1; i < coins.length; i++) {
            System.out.println(i + "\t" + coins[i]);
        }

        int m;
        do {
            System.out.print("Enter 1-6 to Add Money -> "); 
            m = input.nextInt();
            moneyvalues.add(m);
        } while (m != 0);

        addMoney(moneyvalues);

        System.out.println(vendingMachineStock);
        System.out.println("You have $" + money + "\n");

        double moneyremaining = money;
        try {
            for(int selection = 6; selection > 1; selection--) {
                System.out.print("Enter Selection Type Name of Product (0 to exit) -> "); 
                String choice = input.next();
                moneyremaining = moneyremaining - vendingMachineStock.get(choice);
                moneyremaining = Math.round(moneyremaining * 100.0) / 100.0;
                if (moneyremaining < 0) {
                    System.out.println("Insufficient Funds");
                    break;
                }
                System.out.println("You paid " + vendingMachineStock.get(choice) + " for product " + choice);
                selections.add(choice);
                System.out.println("Money Remaining " + moneyremaining);
             } 
        } catch (NullPointerException e) {
            System.out.println("Not a valid selection");
        }
        vendingMachine(selections); 
    }

    public void addMoney(List<Integer> moneyvalues) {
        for (int i = 0; i < moneyvalues.size(); i++) {
            int moneyvalue = moneyvalues.get(i);
            money += coins[moneyvalue];
        }
    }

    public List<String> vendingMachine(List<String> selections) {
        List<String> products = new ArrayList<>(); 

        for (int i = 0; i < selections.size(); i++) {
            double amount = vendingMachineStock.get(selections.get(i));
            money = money - amount;
            if (money >= 0) {
                products.add(selections.get(i));
            } else {
                System.out.println("Insufficient Funds");
                money = money + amount;
            }
            money = Math.round(money * 100.0) / 100.0;
            // System.out.println(money + "\t" + amount);
        }

        int quarters = NumberOfCoins(money, .25);
        money = money - .25 * quarters;
        int dimes = NumberOfCoins(money, .1);
        money = money - .1 * dimes;
        int nickels = NumberOfCoins(money, .05);
        money = money - .05* nickels;
        int pennies = NumberOfCoins(money, .01);
        money = money - .01 * pennies;

        System.out.println("Change returned | " + "Quarters " + quarters + ", " + "Dimes " + dimes + ", " + "Nickels " + nickels + ", " + "Pennies " + pennies);
        System.out.println("Purchased Products | " + products);
        return products;

    }
    
    public static int NumberOfCoins(double amount, double coinamount) {
        int coins = 0;
        while (amount >= coinamount) {
            coins++;
            amount = amount - coinamount;
        }
        return coins;
    }
}