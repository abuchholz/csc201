import java.util.Scanner; // import the Scanner class 

class ProblemOne{  
    public static void main(String args[]){  
        // Create scanner instance
        Scanner input = new Scanner(System.in);
        String page;
        
        // Declare studentID variable int and calculate chapter and problem number
        int studentID = 7416518;
        int chapter = studentID  % 3 + 3;
        int problemFromChapter = 0;
        switch (chapter) {
            case 3:
                problemFromChapter = studentID % 34 + 1;
                break;
            case 4:
                problemFromChapter = studentID % 38 + 1;
                break;
            case 5:
                problemFromChapter = studentID % 46 + 1;
                break;
        }

        // Ask for Page Numbeer Input
        System.out.println("Enter page number"); 
        page = input.nextLine();
        
        // Destroy the instance after use
        input.close();

        //Print response
        System.out.println("Please solve programming exercise " + problemFromChapter + " from chapter " + chapter + ", from page " + page + ".");
    }  
}  