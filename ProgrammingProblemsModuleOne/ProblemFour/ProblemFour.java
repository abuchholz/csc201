// Writing this next time I would have made a Complex Number Class and called that when required to do a specific operation.

import java.util.Scanner; 

class ProblemFour{
    public static void main(String args[]){  
        // Create scanner instance
        Scanner input = new Scanner(System.in);
        
        // Initialize variables needed for operations
        double img1;
        double img2;
        double reg1;
        double reg2;
        int operation;
        
        // Ask for speed and time input
        System.out.println("Some answers may not be simplified down!");
        System.out.print("Imaginary Number 1 (Do Not Include i) -> "); 
        img1 = input.nextDouble();
        System.out.print("Imaginary Number 2 (Do Not Include i) -> "); 
        img2 = input.nextDouble();
        System.out.print("Enter Non Imaginary Number 1 -> "); 
        reg1 = input.nextDouble();
        System.out.print("Enter Non Imaginary Number 2 -> "); 
        reg2 = input.nextDouble();
        System.out.print("\n Addition | 0 \n Subtraction | 1 \n Multiplication | 2 \n Division | 3 \n\n Operation -> "); 
        operation = input.nextInt();
        
        //Find The operation the user requested.
        if(operation == 0) {
           addition(reg1, img1, reg2, img2);
        // Subtraction
        } else if (operation == 1) {
            subtraction(reg1, img1, reg2, img2);
        // Multiplication
        } else if (operation == 2) {
            multiplication(reg1, img1, reg2, img2);
        // Division
        } else {
            division(reg1, img1, reg2, img2);
        }

        // Destroy the instance after use
        input.close();
    }

    // Addition Method
    static void addition(double a1, double a2, double b1, double b2) {
        double regresult = a1 + b1;
        double imgresult = a2 + b2;

        System.out.println(regresult + " + " + imgresult + "i");
    }

    // Subtraction method
    static void subtraction(double a1, double a2, double b1, double b2) {
        double regresult = a1 - b1;
        double imgresult = a2 - b2;

        System.out.println(regresult + " - " + imgresult + "i");
    }

    // Multiplication method use FOIL
    static void multiplication(double a1, double a2, double b1, double b2) {
        double F = a1 * b1;
        double O = a1 * b2;
        double I = a2 * b1;
        double L = a2 * b2;

        double number = F + (L * -1);
        double imaginary = O + I;

        System.out.println(number + " + " + imaginary + "i");
    }

    // Division method
    static void division(double a1, double a2, double b1, double b2) {
        // Top
        double F = a1 * b1;
        double O = a1 * b2;
        double I = a2 * b1;
        double L = a2 * b2;

        //Bottom 
        double Obot = b2 * b2;
        double Lbot = b1 * b1;

        //Results Top
        double number = F + L;
        double imaginary = I - O;
        // Reults Bottom
        double numberbot = Obot + Lbot;

        System.out.println(number + " + " + imaginary + "i");
        System.out.println("_______________");
        System.out.println(numberbot);
    }
}