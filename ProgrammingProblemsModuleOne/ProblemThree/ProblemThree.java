import java.util.Scanner; 
import java.util.Random;
import java.lang.Math; 

class ProblemThree{  
    public static void main(String args[]){  
        // Create scanner/random instance
        Scanner input = new Scanner(System.in);
        int speed;
        int minutes;

        Random randomGenerator = new Random();
        
        // Initialize x and y variables
        int x = 0;
        int y = 0; 
        
        // Ask for speed and time input
        System.out.println("Enter Miles Traveled in 5 Minutes ->"); 
        speed = input.nextInt();
        System.out.println("Enter Time in Minutes - >"); 
        
        // Randomly choose directon
        for(minutes = input.nextInt(); minutes > 0; minutes = minutes-5) {
            int number = randomGenerator.nextInt(4);
            // East
            if(number == 0) {
                x += speed;
            // West
            } else if (number == 1) {
                x -= speed;
            // North
            } else if (number == 2) {
                y += speed;
            // South
            } else {
                y -= speed;
            }
        }
        // Destroy the instance after use
        input.close();

        //Print final destination
        System.out.println(x + ", " + y);

        // Calculation direct distance 
        double result = Math.sqrt(x*x + y*y);
        System.out.println("Direct Distance - " + result);
    }  
}  