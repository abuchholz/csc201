import java.util.Scanner;
import java.text.DecimalFormat;

class DiscussionThirtyOne {
    public static void main(String[] args) {
    // Create scanner instance
    Scanner input = new Scanner(System.in);
    DecimalFormat df = new DecimalFormat("#####.##");

    // Ask user for input 
    System.out.print("Enter the initial deposit amount -> "); 
    double initial = input.nextDouble();
    System.out.print("Enter annual percentage yield -> "); 
    double percentage = input.nextDouble();
    System.out.print("Enter maturity period (number of months) -> "); 
    double stopmonth = input.nextDouble();
    
    System.out.println("Month\tCD Value");
    for(int months = 1; months <= stopmonth; months++) {
        initial = initial + initial * percentage / 100 / 12;
        System.out.println(months + "\t" + df.format(initial));
    }
    
    // Destroy the instance after use
    input.close();
    }
}
